## Julien Moatti

I am currently an assistant professor (maître de conférence) in applied mathematics at the [ENSEIRB-MATMECA](https://enseirb-matmeca.bordeaux-inp.fr/fr) (Bordeaux INP).

I am also affiliated to the Scientific Computing and Modelisation team of the [IMB laboratory](https://www.math.u-bordeaux.fr/imb/).

I am working on numerical analysis, and I am interested in structure-preserving schemes for parabolic PDEs.

My profesionnal webpage is there: https://sites.google.com/view/julien-moatti/

Mail: julien(dot)moatti(at)ipb(dot)fr <br>
ORCID: [0000-0002-0271-4571](https://orcid.org/0000-0002-0271-4571)<br>
Gitlab: [Julien_Moatti](https://gitlab.com/Julien_Moatti)